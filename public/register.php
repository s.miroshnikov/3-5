<?php

session_start();

$_SESSION['login'] = 'new';

$login = $_POST['login'];
$pass = $_POST['pass'];

$user = 'u20404';
$dbpass = '4364822';
$host = 'localhost';
$dbname = 'u20404';

$link = new mysqli($host, $user, $dbpass, $dbname);

if(isset($_POST['submit']))
{
$err = [];

if(!preg_match("/^[a-zA-Z0-9]+$/",$_POST['login']))
{
$err[] = "Логин может состоять только из букв английского алфавита и цифр";
}

if(strlen($_POST['login']) < 3 or strlen($_POST['login']) > 30)
{
    $err[] = "Логин должен быть не меньше 3-х символов и не больше 30";
}

if(strlen($_POST['pass']) < 5){
    $err[] = "Пароль должен состоять хотя бы из 5 символов";
}

$query = mysqli_query($link, "SELECT user_id FROM users WHERE user_login='".mysqli_real_escape_string($link, $_POST['login'])."'");

if(mysqli_num_rows($query) > 0)
{
    $err[] = "Пользователь с таким логином уже существует в базе данных";
?>
    <a href="register.php">НАЗАД</a>
    <a href="login.php">ВОЙТИ</a>
<?php
}
$lpass=md5($_POST['pass']);
    if(count($err) == 0) {
        try {
            $db = new PDO('mysql:host=localhost; dbname=u20404', $user, $dbpass);
            echo "Connected successfully<br>";
        } catch (PDOException $error) {
            echo 'Connection error:';
        }

        try {
            $data = array('login' => $login, 'pass' => $lpass);
            $stmt = $db->prepare("INSERT INTO users (user_login, user_password) VALUES (:login, :pass)");
            $stmt->bindParam(':login', $login);
            $stmt->bindParam(':pass', $lpass);
            $stmt->execute();
        } catch (PDOException $e) {
            exit();
        }
        header("Location: index.html");
    }
    else
    {
        print "<b>При регистрации произошли следующие ошибки:</b><br>";
        foreach($err AS $error)
        {
            print $error."<br>";
        }
    }
}
?>

<div>
    <form method="post">
        <label class="reg-style">Username:<br>
            <input type="text" name="login"/></label>
        <br>
        <label class="reg-style">Password:<br>
            <input type="password" name="pass"/></label>
        <br>
        <input class="reg-button" type="submit" name="submit" value="Зарегистрироваться"/>
        <br><br>
        <a id="main" href="main.html">На главную</a>
    </form>
</div>

<style>
    *{
        margin: 0;
        padding: 0;
    }

    html{
        background-color: #ffffff;
        font-family:Roboto Light, sans-serif;
    }

    div{
        text-align: center;
    }

    .reg-style{
        font-family:Roboto Light, sans-serif;
        font-weight: bold;
        font-size: 150%;
    }

    .reg-button {
        min-width: 100px;
        font-family: inherit;
        appearance: none;
        border: 0;
        border-radius: 2px;
        background: #474747;
        color: #fff;
        padding: 3px 16px;
        font-size: 1rem;
        cursor: pointer;
        text-align: center;
        position: relative;
        transform: translate(0, 50%);
    }

    .reg-button:hover {
        background: #797979;
    }

    .reg-button:focus {
        outline: none;
        box-shadow: 0 0 0 4px #cbd6ee;
    }

    #main{
        text-decoration: none;
        font-weight: bolder;
        color:black;
    }

    #main:hover{
        color: #000000;
        text-shadow: 1px 1px 1px #404040;
    }
</style>
