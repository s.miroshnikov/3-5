<div>
    <form class="enter-form">
        <label><b>В первый раз? Вам нужно зарегистрироваться!</b>
            <br>
            <a class="enter-link" href="register.php"><b>Регистрироваться</b></a>
        </label>
        <label><b>Хотите изменить информацию?<b></b>
                <br>
                <a class="enter-link" href="login.php">Хочу изменить информацию</a>
        </label>
    </form>
</div>

<style>
    *{
        margin: 0;
        padding: 0;
    }

    html{
        background-color: #ffffff;
        font-family:Roboto Light, sans-serif;
    }

    div{
        text-align: center;
    }

    .enter-form{
        display: flex;
        align-items: center;
        justify-content: space-evenly;
    }
    .enter-link{
        text-decoration: none;
        font-size: larger;
        color: #b32f2f;
    }

    .enter-form:focus{
        background: #6e2020;
    }
    .enter-link:hover{
        color: #000000;
        text-shadow: 1px 1px 1px 2px #4f1717;
    }
</style>
