<?php

session_start();

$login = $_SESSION['login'];

$name = $_POST['name'];
$email = $_POST['email'];
$dateofbirth = $_POST['dateOfBirth'];
$sop = $_POST['first_radio_group'];
$col = $_POST['second_radio_group'];
$superpower = $_POST['superpower'];
$aboutperson = $_POST['message'];
header('Content-Type: text/html; charset=UTF-8');

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    $messages = array();

    if (!empty($_COOKIE['save'])) {
        setcookie('save', '', 100000);
        $messages[] = 'Спасибо, результаты сохранены.';
    }

    // Складываем признак ошибок в массив.
    $errors = array();
    $errors['name'] = !empty($_COOKIE['name_error']);
    $errors['email'] = !empty($_COOKIE['email_error']);
    $errors['dateOfBirth'] = !empty($_COOKIE['dateOfBirth_error']);
    $errors['contract'] = !empty($_COOKIE['contract_error']);


    // Выдаем сообщения об ошибках.
    if ($errors['name']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('name_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div class="error">Заполните имя.</div>';
    }
    if ($errors['email']) {
        setcookie('email_error', '', 100000);
        $messages[] = '<div class="error">Заполните email.</div>';
    }
    if ($errors['dateOfBirth']) {
        setcookie('dateOfBirth_error', '', 100000);
        $messages[] = '<div class="error">Заполните дату рождения.</div>';
    }
    if ($errors['contract']) {
        setcookie('contract_error', '', 100000);
        $messages[] = '<div class="error">Вы должны ознакомиться с контрактом.</div>';
    }

    // Складываем предыдущие значения полей в массив, если есть.
    $values = array();
    $values['name'] = empty($_COOKIE['name_value']) ? '' : $_COOKIE['name_value'];
    $values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
    $values['dateOfBirth'] = empty($_COOKIE['dateOfBirth_value']) ? '' : $_COOKIE['dateOfBirth_value'];
    $values['contract'] = empty($_COOKIE['contract_value']) ? '' : $_COOKIE['contract_value'];


}

// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.
else {
    // Проверяем ошибки.
    $errors = FALSE;
    if (empty($_POST['name'])) {
        setcookie('name_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        $name = $_POST["name"];
        if (!preg_match("^[А-Яа-яЁё\s]+$|^[a-zA-Z]+$", $name)) {
            $nameErr = "Only letters";
            setcookie('name_value', $_POST['name'], time() + 30 * 24 * 60 * 60);
        }
        if (empty($_POST['email'])) {
            setcookie('email_error', '1', time() + 24 * 60 * 60);
            $errors = TRUE;
        } else {
            $email = $_POST["email"];
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $email_error = "Invalid email format";
            } else setcookie('email_value', $_POST['email'], time() + 30 * 24 * 60 * 60);
        }
        if (empty($_POST['dateOfBirth'])) {
            setcookie('dateOfBirth_error', '1', time() + 24 * 60 * 60);
            $errors = TRUE;
        } else {
            setcookie('dateOfBirth_value', $_POST['dateOfBirth'], time() + 30 * 24 * 60 * 60);
        }
        if (empty($_POST['contract'])) {
            setcookie('contract_error', '1', time() + 24 * 60 * 60);
            $errors = TRUE;
        } else {
            setcookie('contract_value', $_POST['contract'], time() + 30 * 24 * 60 * 60);
        }

        if ($errors) {
            header('Location: index.php');
            exit();
        } else {
            setcookie('name_error', '', 100000);
        }

        if ($errors) {
            header('Location: index.php');
            exit();
        } else {
            setcookie('email_error', '', 100000);
        }
        if ($errors) {
            header('Location: index.php');
            exit();
        } else {
            setcookie('dateOfBirth_error', '', 100000);
        }
        if ($errors) {
            header('Location: index.php');
            exit();
        } else {
            setcookie('contract_error', '', 100000);
        }

        setcookie('save', '1');
        header('Location: index.php');


// Сохранение в базу данных.

        $user = 'u20404';
        $pass = '4364822';
        $sql = 'mysql:host=localhost; dbname=u20404';
        $db = new PDO($sql, $user, $pass);
        

        try {
            $db = new PDO($sql, $user, $pass);
            echo "Connected successfully<br/>";
        } catch (PDOException $error) {
            echo 'Connection error:' . $error->getMessage();
        }



        if (!empty($_COOKIE[session_name()]) && session_start() && !empty($_SESSION['login']) && $_SESSION['login']!='new') {
            try {
                $data = array('name' => $name, 'email' => $email, 'dateofbirth' => $dateofbirth, 'sop' => $sop, 'col' => $col, 'superpower' => $superpower, 'message' => $aboutperson);
                $stmt = $db->prepare("UPDATE person SET name = :name, email = :email, dateofbirth = :dateofbirth, sop = :sop, col = :col, superpower = :superpower, aboutperson = :aboutperson WHERE person.id = (SELECT user_id FROM users WHERE user_login = '$login')");
                $stmt->bindParam(':name', $name);
                $stmt->bindParam(':email', $email);
                $stmt->bindParam(':dateofbirth', $dateofbirth);
                $stmt->bindParam(':sop', $sop);
                $stmt->bindParam(':col', $col);
                $stmt->bindParam(':superpower', $superpower);
                $stmt->bindParam(':aboutperson', $aboutperson);
                $stmt->execute();
            } catch (PDOException $e) {
                print('PDOError : ' . $e->getMessage());
                exit();
            }
        }else if ($_SESSION['login']=='new'){
            try {
                $data = array('name' => $name, 'email' => $email, 'dateofbirth' => $dateofbirth, 'sop' => $sop, 'col' => $col, 'superpower' => $superpower, 'message' => $aboutperson);
                $stmt = $db->prepare("INSERT INTO person (name, email, dateofbirth, sop, col, superpower, aboutperson) VALUES (:name, :email, :dateofbirth, :sop, :col, :superpower, :aboutperson)");
                $stmt->bindParam(':name', $name);
                $stmt->bindParam(':email', $email);
                $stmt->bindParam(':dateofbirth', $dateofbirth);
                $stmt->bindParam(':sop', $sop);
                $stmt->bindParam(':col', $col);
                $stmt->bindParam(':superpower', $superpower);
                $stmt->bindParam(':aboutperson', $aboutperson);
                $stmt->execute();
            } catch (PDOException $e) {
                print('PDOError : ' . $e->getMessage());
                exit();
            }
        }
        setcookie('login', $login);
    }
}

